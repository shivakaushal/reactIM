## reactIM -- instant messenger powered by react and firebase

To get started,

* clone app
```
git clone https://gitlab.com/shivakaushal/reactIM.git reactIM
```

* Install dependencies
```
cd reactIM && npm install
```

* Start dev server
```
npm start
```

* Open browser at `http://localhost:8080/` to view app
import axios from 'axios';
import _ from 'lodash';
import firebase from 'firebase/app';
import 'firebase/firestore';

/**
 * Configure axios instance to call API
 */
const api = axios.create({
  baseURL: '/',
  timeout: 30000,
  headers: {
    post: {
      'Content-Type': 'application/json',
    },
    common: {
      'Accept': 'application/json', // eslint-disable-line quote-props
    },
    'X-Requested-With': 'XMLHttpRequest',
  },
});

api.interceptors.response.use((response) => {
  // Do something with response data
  return response;
}, (error) => {
  // Do something with response error
  if (error.response) {
    if (error.response.status && error.response.status === 401) {
      //Redirect to root page as there is no session
      window.location.assign('/');
    }

    if (error.response.status === 404) {
      error.response = createGlobalErrorObj();
    }
  } else {
    error.response = createGlobalErrorObj();
  }

  return Promise.reject(error);
});

export default api;

export function setApiDefaults(defaults, api) {
  (api || axios).defaults = _.extend(axios.defaults, defaults);
}

export const createGlobalErrorObj = (errorMessages = []) => {
  let newError = _.cloneDeep(errorObj);

  errorMessages.forEach((err, i) => {
    newError.data.error[i].fields[0].messages[0].text = err;
  });

  return newError;
};

export const errorObj = {
  data: {
    error: [
      {
        name: 'global',
        fields: [
          {
            name: 'global',
            messages: [
              {
                text: 'An internal error occurred and we are unable to complete your request.',
              },
            ],
          },
        ],
      },
    ],
  },
};

// TODO: rotate keys and move to local env variable
firebase.initializeApp({
  apiKey: 'AIzaSyDBuzlgyX9PAv3mPrsbemhfJVfEnjLCk6A',
  authDomain: 'instantchatapp-12aab.firebaseapp.com',
  projectId: 'instantchatapp-12aab',
});

// Initialize Cloud Firestore through Firebase
export const db = firebase.firestore();
const settings = {timestampsInSnapshots: true};
db.settings(settings);

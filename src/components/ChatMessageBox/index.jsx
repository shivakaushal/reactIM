import React from 'react';
import * as moment from 'moment';

import './style.scss';

const ChatMessageBox = (props) => {
  const {
    messages,
    users,
  } = props;

  return (
    <div data-component={'ChatMessageBox'}>
      {(messages.length < 1)
        ? (
          <h1 className={'text-muted'}>
            Let's start a conversation.
          </h1>
        )
        : (
          <div className={'scrollBox'}>
            {
              messages.sort((a, b) => {
                if (a.timestamp.seconds === b.timestamp.seconds) {
                  return 0;
                }

                return (a.timestamp.seconds < b.timestamp.seconds) ? -1 : 1;
              }).map((m, i) => {
                return (
                  <p key={i}>
                    <strong>@{users[m.from]}:</strong>
                    <span className={'messageText'}>
                      {m.message}
                    </span>
                    <br/>
                    <small className={'text-muted'}>{moment.unix(m.timestamp.seconds).fromNow()}</small>
                  </p>
                );
              })
            }
          </div>
        )
      }

    </div>
  );
};

export default ChatMessageBox;

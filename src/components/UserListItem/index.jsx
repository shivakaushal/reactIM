import React from 'react';

import './style.scss';

const UserListItem = (props) => {
  const {user} = props;

  return (
    <div data-component={'UserListItem'}>
      <img src={`https://ui-avatars.com/api?name=${user.username}&size=64`} className={'float-left'} />
      {user.username}
      <div className={'clearfix'}></div>
    </div>
  );
};

export default UserListItem;

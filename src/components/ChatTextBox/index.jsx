import React from 'react';

import './style.scss';

const ChatTextBox = (props) => {
  const {
    textMessage,
    handleTextInput,
    handleBtnClick,
  } = props;

  return (
    <div data-component={'ChatTextBox'}>
      <form className={'form-inline'}>
        <div className={'col col-sm-10'}>
          <textarea
            className={'form-control'}
            rows={1}
            onChange={handleTextInput}
            value={textMessage}
          />
        </div>
        <div className={'col col-sm-2'}>
          <button type={'submit'} className={'btn btn-primary btn-block'} onClick={handleBtnClick}>
            SEND
          </button>
        </div>
      </form>
    </div>
  );
};

export default ChatTextBox;

import {applyMiddleware, compose, createStore} from 'redux';
import {createBrowserHistory} from 'history';
import {routerMiddleware, connectRouter} from 'connected-react-router';
import {createLogger} from 'redux-logger';
import thunk from 'redux-thunk';

/** Import combined reducers */
import rootReducer from 'reducers';

/** Handle ajax requests */
import api from './utils';

export const history = createBrowserHistory();

let middlewares = (process.env.NODE_ENV !== 'production')
  ? [thunk.withExtraArgument(api), createLogger()]
  : [thunk.withExtraArgument(api)];

middlewares = [...middlewares, routerMiddleware(history)];

const store = createStore(
  connectRouter(history)(rootReducer),
  compose(applyMiddleware(...middlewares)),
);

export default store;

if (module.hot) {
  module.hot.accept('./reducers', () => store.replaceReducer(connectRouter(history)(rootReducer)));
}

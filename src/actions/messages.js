import {db} from '../utils';

/**
 * Action types
 */
export const MESSAGE_SUCCESS  = 'MESSAGE_SUCCESS';
export const MESSAGES_ERROR  = 'MESSAGES_ERROR';
export const MESSAGES_REQUEST = 'MESSAGES_REQUEST';

export function messagesRequest() {
  return {
    type: MESSAGES_REQUEST,
  };
}

export function messagesError(error) {
  return {
    type: MESSAGES_ERROR,
    error: error.response,
  };
}

export function getMessagesSuccess(data) {
  return {
    type: MESSAGE_SUCCESS,
    payload: data,
  };
}

export function getMessages(currentUserId, selectedUserId) {
  return function (dispatch, getState, api) {
    dispatch(messagesRequest());

    const currentUserRef = db.collection('users').doc(currentUserId);
    const selectedUserRef = db.collection('users').doc(selectedUserId);

    const queryMessagesFrom = db.collection('messages')
      .where('from', '==', currentUserRef)
      .where('to', '==', selectedUserRef).get();

    const queryMessagesTo = db.collection('messages')
      .where('from', '==', selectedUserRef)
      .where('to', '==', currentUserRef).get();

    Promise.all([
      queryMessagesFrom,
      queryMessagesTo,
    ])
    .then((querySnapshots) => {
      let messageListFrom = [];
      querySnapshots[0].forEach((doc) => {
        const data = doc.data();
        messageListFrom = [...messageListFrom, data];
      });

      let messageListTo = [];
      querySnapshots[1].forEach((doc) => {
        const data = doc.data();
        messageListTo = [...messageListTo, data];
      });

      dispatch(getMessagesSuccess([...messageListFrom, ...messageListTo]));
    })
    .catch((error) => dispatch(messagesError(error)));
  };
}

export function sendMessage(currentUserId, selectedUserId, message) {
  return function (dispatch, getState, api) {
    dispatch(messagesRequest());

    const currentUserRef = db.collection('users').doc(currentUserId);
    const selectedUserRef = db.collection('users').doc(selectedUserId);

    db.collection('messages').add({
      from: currentUserRef,
      to: selectedUserRef,
      timestamp: new Date(),
      message,
    })
    .then(() => dispatch(getMessages(currentUserId, selectedUserId)))
    .catch((error) => dispatch(messagesError(error)));
  };
}

export function watchMessages(currentUserId, selectedUserId) {
  return function (dispatch, getState, api) {
    dispatch(messagesRequest());

    db.collection('messages').onSnapshot((doc) => {
      dispatch(getMessages(currentUserId, selectedUserId));
    });
  };
}

import {push} from 'connected-react-router';

import {db} from '../utils';

/**
 * Action types
 */
export const GET_USERS_SUCCESS   = 'GET_USERS_SUCCESS';
export const CREATE_USER_SUCCESS   = 'CREATE_USER_SUCCESS';
export const USERS_ERROR     = 'USERS_ERROR';
export const USERS_REQUEST   = 'USERS_REQUEST';

export function usersRequest() {
  return {
    type: USERS_REQUEST,
  };
}

export function usersError(error) {
  return {
    type: USERS_ERROR,
    error: error.response,
  };
}

export function getUsersSuccess(data) {
  return {
    type: GET_USERS_SUCCESS,
    payload: data,
  };
}

export function createUserSuccess(data) {
  return {
    type: CREATE_USER_SUCCESS,
    payload: data,
  };
}

export function getUsers() {
  return function (dispatch, getState, api) {
    dispatch(usersRequest());

    return db.collection('users').get()
      .then((querySnapshot) => {
        let userList = [];
        querySnapshot.forEach((doc) => {
          const data = doc.data();
          userList = [...userList, {id: doc.id, username: data.username}];
        });

        dispatch(getUsersSuccess(userList));
      })
      .catch((error) => dispatch(usersError(error)));
  };
}

export function watchUsers() {
  return function (dispatch, getState, api) {
    dispatch(usersRequest());

    db.collection('users').onSnapshot((doc) => {
      dispatch(getUsers());
    });
  };
}


export function createUser(user) {
  return function (dispatch, getState, api) {
    dispatch(usersRequest());

    if (!user.username.length) {
      dispatch(usersError(new Error('Username cannot be empty')));
      return;
    }

    // add the username if it doesn't exist and and fetch it to confirm insert/exists
    return db.collection('users').where('username', '==', user.username).get()
      .then((querySnapshot) => {
        if (querySnapshot.empty) {
          return db.collection('users').add(user);
        }
        if (querySnapshot.docs.length) {
          const doc = querySnapshot.docs[0];
          const data = doc.data();
          dispatch(createUserSuccess({id: doc.id, username: data.username}));
          throw null;
        }
      })
      .then((doc) => {
        if (doc && doc.path) {
          return db.doc(doc.path).get();
        }
      })
      .then((doc) => {
        if (doc && doc.exists) {
          dispatch(createUserSuccess({id: doc.id, ...doc.data()}));
        } else {
          dispatch(usersError(false));
        }
      })
      .catch((error) => {
        if (error) {
          dispatch(usersError(error));
        }
      });
  };
}

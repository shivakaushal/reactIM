import {Map, List} from 'immutable';

import {
  USERS_REQUEST,
  USERS_FAILURE,
  GET_USERS_SUCCESS,
  CREATE_USER_SUCCESS,
} from '../actions/users';

export function setUsersRequest(state) {
  const metadata = state.get('_metadata');
  return state.merge({
    _metadata: metadata.merge({
      inProgress: true,
      error: false,
    }),
  });
}

export function setUsersError(state, action) {
  const {
    error,
  } = action;

  return state.mergeDeep({
    _metadata: Map({
      inProgress: false,
      error: error.data.error,
    }),
  });
}

export function setUserListSuccess(state, action) {
  const {
    payload,
  } = action;

  let userList = [];

  const metadata = state.get('_metadata');
  const currentUser = state.get('currentUser');

  if (currentUser) {
    userList = payload.filter((ea) => (ea.id != currentUser.toJS().id));
  }

  return state.merge({
    userList: List(userList),
    _metadata: metadata.merge({
      inProgress: false,
    }),
  });
}

export function setUserSuccess(state, action) {
  const {
    payload,
  } = action;

  const metadata = state.get('_metadata');

  return state.merge({
    currentUser: Map(payload),
    _metadata: metadata.merge({
      inProgress: false,
    }),
  });
}

export default function (state = Map({
  userList: List(),
  _metadata: Map({
    inProgress: false,
    error: false,
  }),
}), action) {
  switch (action.type) {
    case USERS_REQUEST:
      return setUsersRequest(state, action);

    case USERS_FAILURE:
      return setUsersError(state, action);

    case GET_USERS_SUCCESS:
      return setUserListSuccess(state, action);

    case CREATE_USER_SUCCESS:
      return setUserSuccess(state, action);

    default:
      return state;
  }
}

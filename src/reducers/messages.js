import {Map, List} from 'immutable';

import {
  MESSAGE_REQUEST,
  MESSAGE_FAILURE,
  MESSAGE_SUCCESS,
} from '../actions/messages';

export function setChatMessageRequest(state) {
  const metadata = state.get('_metadata');
  return state.merge({
    _metadata: metadata.merge({
      inProgress: true,
      error: false,
    }),
  });
}

export function setChatMessageError(state, action) {
  const {
    error,
  } = action;

  return state.mergeDeep({
    _metadata: Map({
      inProgress: false,
      error: error.data.error,
    }),
  });
}

export function setChatMessageSuccess(state, action) {
  const {
    payload,
  } = action;

  const metadata = state.get('_metadata');
  const messages = payload.map((conv) => ({
    from: conv.from.id,
    to: conv.to.id,
    timestamp: conv.timestamp,
    message: conv.message,
  }));

  return state.merge({
    messages: List(messages),
    _metadata: metadata.merge({
      inProgress: false,
    }),
  });
}

export default function (state = Map({
  messages: List(),
  _metadata: Map({
    inProgress: false,
    error: false,
  }),
}), action) {
  switch (action.type) {
    case MESSAGE_REQUEST:
      return setChatMessageRequest(state, action);

    case MESSAGE_FAILURE:
      return setChatMessageError(state, action);

    case MESSAGE_SUCCESS:
      return setChatMessageSuccess(state, action);

    default:
      return state;
  }
}

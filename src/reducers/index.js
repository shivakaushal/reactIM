import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';

import users from './users';
import messages from './messages';

export default combineReducers({
  users,
  messages,
  router: routerReducer,
});

import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'connected-react-router';
import {HashRouter, Switch, Route} from 'react-router-dom'

import store, {history} from 'reduxStore.js';

import WelcomeWindow from 'containers/WelcomeWindow';
import ChatWindow from 'containers/ChatWindow';

import 'styles/_vendor.scss';
import 'styles/_global.scss';

ReactDOM.render((
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <HashRouter>
        <div className={'container-fluid h-100'}>
          <div className={'row h-100'}>
            <Switch>
              <Route exact path={'/'} component={WelcomeWindow} />
              <Route path={'/:userID'} component={ChatWindow} />
            </Switch>
          </div>
        </div>
      </HashRouter>
    </ConnectedRouter>
  </Provider>
), document.getElementById('app'));

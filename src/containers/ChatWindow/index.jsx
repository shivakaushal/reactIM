import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {push} from 'connected-react-router';

import UserList from 'containers/UserList';
import ChatTextBox from 'components/ChatTextBox';
import ChatMessageBox from 'components/ChatMessageBox';

import {getUsers, watchUsers} from 'actions/users';
import {getMessages, sendMessage, watchMessages} from 'actions/messages';

import './style.scss';

class ChatWindow extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      textMessage: '',
      selectedUser: false,
    };

    this.handleBtnClick = this.handleBtnClick.bind(this);
    this.handleTextInput = this.handleTextInput.bind(this);
  }

  componentDidMount() {
    const {
      currentUser,
      dispatchGetUsers,
      dispatchWatchUsers,
      history,
    } = this.props;

    if (!currentUser) {
      history.push('/');
    } else {
      dispatchGetUsers();
      dispatchWatchUsers();
    }
  }

  componentDidUpdate(prevProps) {
    const {
      location,
      userList,
      currentUser,
      dispatchGetMessages,
      dispatchWatchMessages,
    } = this.props;

    if (this.props.location.pathname !== prevProps.location.pathname) {
      const selectedUserId = location.pathname.replace('/', '');
      const selectedUser = userList.find((ea) => (ea.id == selectedUserId));

      if (currentUser && !!selectedUser) {
        this.setState({selectedUser}, () => {
          dispatchGetMessages(currentUser.id, selectedUser.id);
          dispatchWatchMessages(currentUser.id, selectedUser.id);
        });
      }
    }
  }

  handleTextInput(e) {
    this.setState({textMessage: e.target.value});
  }

  handleBtnClick(e) {
    const {
      currentUser,
      dispatchSendMessage,
    } = this.props;

    const {
      textMessage,
      selectedUser,
    } = this.state;

    e.preventDefault();
    this.setState({textMessage: ''}, () => dispatchSendMessage(currentUser.id, selectedUser.id, textMessage));
  }

  render() {
    const {
      currentUser,
      userList,
      messages,
      dispatchCreateUser,
      location,
    } = this.props;

    const {
      selectedUser,
    } = this.state;

    let showChatWindow = false;
    let usersDict = [];
    if (currentUser && !!selectedUser) {
      showChatWindow = this.state.selectedUser.id !== currentUser.id;
      [currentUser, selectedUser].forEach((ea) => usersDict[ea.id] = ea.username);
    }

    return [
      <UserList key={1} {...this.props} userList={userList} />,
      <div data-component={'ChatWindow'} className={'col col-sm-9 col-md-10'} key={2}>
        {showChatWindow
          ? (
            <div>
              <ChatMessageBox messages={messages} users={usersDict}/>

              <ChatTextBox
                handleBtnClick={this.handleBtnClick}
                handleTextInput={this.handleTextInput}
                textMessage={this.state.textMessage}
              />
            </div>)
          : <div className={'welcomeText'}><h1> Welcome to reactIM </h1><p className={'lead'}>Hi <strong>{currentUser.username}</strong>! Click on a user from the list to start chatting 💬.</p></div>
        }
      </div>,
    ];
  }
}

ChatWindow.propTypes = {
  currentUser: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]).isRequired,
  userList: PropTypes.array.isRequired,
  messages: PropTypes.array.isRequired,
  dispatchGetUsers: PropTypes.func.isRequired,
  dispatchWatchUsers: PropTypes.func.isRequired,
  dispatchGetMessages: PropTypes.func.isRequired,
  dispatchSendMessage: PropTypes.func.isRequired,
  dispatchWatchMessages: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  const {
    users,
    messages,
  } = state;

  return {
    currentUser: users.has('currentUser') ? users.get('currentUser').toJS() : false,
    userList: users.get('userList').toJS(),
    messages: messages.get('messages').toJS(),
  };
};

const mapDispatchToProps = (dispatch) => ({
  dispatchGetUsers: (...args) => dispatch(getUsers(...args)),
  dispatchWatchUsers: (...args) => dispatch(watchUsers(...args)),
  dispatchGetMessages: (...args) => dispatch(getMessages(...args)),
  dispatchSendMessage: (...args) => dispatch(sendMessage(...args)),
  dispatchWatchMessages: (...args) => dispatch(watchMessages(...args)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ChatWindow);

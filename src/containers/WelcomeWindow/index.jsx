import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {createUser} from 'actions/users';

import './style.scss';

class WelcomeWindow extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
    };
  }

  componentDidUpdate() {
    const {
      currentUser,
      history,
    } = this.props;

    if (currentUser) {
      history.push(currentUser.id);
    }
  }

  render() {
    const {
      currentUser,
      dispatchCreateUser
    } = this.props;

    return (
      <div data-component={'WelcomeWindow'} className={'col col-sm-6 col-md-4'}>
        <h1> Welcome to reactIM </h1>

        {!currentUser &&
          <form onSubmit={(e) => {
            e.preventDefault();
            dispatchCreateUser(this.state);
          }}>
            <p className={'lead'}>Enter your username to get started 🚀</p>

            <div className={'form-group'}>
              <label htmlFor={'username'}>Username</label>
              <input
                type={'text'}
                className={'form-control'}
                id={'username'}
                placeholder={'Make it memorable!'}
                onChange={(e) => this.setState({username: e.target.value})}
                required
              />
            </div>

            <input type={'submit'} value={'GET STARTED'} className={'btn btn-primary'}/>
          </form>
        }
      </div>
    );
  }
}

WelcomeWindow.propTypes = {
  currentUser: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]).isRequired,
  dispatchCreateUser: PropTypes.func.isRequired,

};

const mapStateToProps = (state) => {
  const {users} = state;

  return {
    currentUser: users.has('currentUser') ? users.get('currentUser').toJS() : false,
  };
};

const mapDispatchToProps = (dispatch) => ({
  dispatchCreateUser: (...args) => dispatch(createUser(...args)),
});

export default connect(mapStateToProps, mapDispatchToProps)(WelcomeWindow);

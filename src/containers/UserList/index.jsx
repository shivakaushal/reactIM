import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Link} from  'react-router-dom';

import UserListItem from 'components/UserListItem';

import './style.scss';

class UserList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userList: [],
    };

    this._filterUserList = this._filterUserList.bind(this);
  }

  _filterUserList(e) {
    const searchString = e.target.value;
    this.setState({
      userList: this.props.userList.filter((ea) => (ea.username.indexOf(searchString) > -1)),
    });
  }

  componentDidUpdate(prevProps) {
    const {userList} = this.props;

    if (userList.length != prevProps.userList.length) {
      this.setState({userList});
    }
  }

  render() {
    const {userList} = this.state;

    const selectedUserId = this.props.location.pathname.replace('/', '');

    return (
      <div data-component={'UserList'} className={'col col-sm-3 col-md-2'}>
        <div className={'listHeader'}>
          <div className={'form-group'}>
            <input
              type={'text'}
              className={'form-control'}
              placeholder={'Filter Users'}
              onChange={this._filterUserList}
              required
            />
          </div>
        </div>
        <div className={'listContent'}>
          <div className={'scrollBox'}>
            {(userList.length > 0)
              ? userList.map((u, i) => (
                <Link key={i} to={`/${u.id}`} className={`${(u.id == selectedUserId) ? 'active' : ''}`}>
                  <UserListItem user={u}/>
                </Link>
              ))
              : <p>No users found.</p>
            }
          </div>
        </div>
      </div>
    );
  }
}

UserList.propTypes = {
  userList: PropTypes.array.isRequired,
};

export default UserList;
